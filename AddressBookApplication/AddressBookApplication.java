import java.io.*;

public class AddressBookApplication {

	static public void main(String args[]) throws IOException {
		
		String[] entry = new String[8];
		AddressBook contactList = new AddressBook();
		int count = 0;
		
		FileReader file_input = new FileReader("AddressInputDataFile.txt");
		
		BufferedReader BR = new BufferedReader(file_input);
		
		String temp = "";
		while((temp = BR.readLine()) != null){
			entry[count] = temp;
			count++;
			if (count == 8){
				count = 0;
				contactList.newEntry(entry[0], entry[1], entry[2], entry[3], entry[4], entry[5], entry[6], entry[7]);
			}
		}
		
		contactList.list();
		
	}
	
}