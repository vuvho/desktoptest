/*
 * Class:AddressCard
 * By: Andy Ho
 * Date: 1/13/2016
 * Purpose: Prompts for Address Elements from Menu class and records it as
 * an object of AddressCard
 */
import java.util.Scanner;
public class AddressCard {
	
	private String firstName; //These are all the variables to hold the address info
	private String lastName;
	private String street;
	private String city;
	private String state;
	private String zip;
	private String telephone;
	private String email;
	
	public AddressCard(){
		Scanner input = new Scanner(System.in);
		System.out.println(Menu.prompt_FirstName()); //Asks for first name in console
		firstName = input.nextLine(); //Puts it in corresponding variable
		System.out.println(Menu.prompt_LastName()); //Asks for last name in console
		lastName=input.nextLine();
		System.out.println(Menu.prompt_Street()); // Asks for street in console
		street=input.nextLine();
		System.out.println(Menu.prompt_City()); // Asks for City in console
		city=input.nextLine();
		System.out.println(Menu.prompt_State()); // Asks for State in console
		state=input.nextLine();
		System.out.println(Menu.prompt_Zip()); // Asks for Zip in console
		zip=input.nextLine();
		System.out.println(Menu.prompt_Telephone()); // Asks for Telephone in console
		telephone=input.nextLine();
		System.out.println(Menu.prompt_Email()); // Asks for Email in console
		email=input.nextLine();
	}	

	public String toString(){ // Basically the print function. Returns a long string with all the info
		String temp;
		temp = "First Name: " + this.firstName + "\nLast Name: " + this.lastName +
				"\nStreet: " + this.street + "\nCity: " + this.city + 
				"\nState: " + this.state + "\nZip: " + this.zip +
				"\nTelephone: " + this.telephone + "\nEmail: " + this.email + "\n";
		return temp;
	}
}
