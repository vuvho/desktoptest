
public class AddressEntry {
	private String firstName, lastName, street, city, state, zip, phone, email;
	
	public void setFirstName(String first){ this.firstName = first;}
	public void setLastName (String last){ this.lastName = last;}
	public void setStreet (String streetName){ this.street = streetName;}
	public void setCity (String cityName){ this.state = cityName;}
	public void setState (String stateName){ this.state = stateName;}
	public void setZip (String zipCode){ this.zip = zipCode;}
	public void setPhone (String phoneNumber) { this.phone = phoneNumber;}
	public void setEmail (String emailAddress) { this.email = emailAddress;}
	
	public String getFirstName (){return this.firstName;}
	public String getLastName(){return this.lastName;}
	public String getStreet(){return this.street;}
	public String getCity(){return this.city;}
	public String getState(){return this.state;}
	public String getZip(){return this.zip;}
	public String getEmail(){return this.email;}
	
	public AddressEntry(String first, String last, String streetName, String cityName, String stateName, String zipCode, String emailAddress, String phoneNumber){
		setFirstName(first);
		setLastName (last);
		setStreet (streetName);
		setCity (cityName);
		setState (stateName);
		setZip (zipCode);
		setPhone (phoneNumber);
		setEmail (emailAddress);
	}
	
	public String toString(){
		String temp;
		temp = Menu.prompt_FirstName() + this.firstName + "\n";
		temp += Menu.prompt_LastName() + this.lastName + "\n";
		temp += Menu.prompt_Street() + this.street + "\n";
		temp += Menu.prompt_City() + this.city + "\n";
		temp += Menu.prompt_State() + this.state + "\n";
		temp += Menu.prompt_Zip() + this.zip + "\n";
		temp += Menu.prompt_Telephone() + this.phone + "\n";
		temp += Menu.prompt_Email() + this.email + "\n";
		return temp;
	}
	
}
