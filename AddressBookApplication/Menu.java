/* 
 * Class: Menu
 * By: Andy Ho
 * Date 1/12/2016
 * 
 * Purpose: This Menu Class contains prompts where when called will return a string
 * 
 */
public class Menu {

	static public String prompt_FirstName(){ //When called, returns string for console
		return "First Name: ";
	}
	static public String prompt_LastName(){ //When called, returns string for console
		return "Last Name:  ";
	}
	static public String prompt_Street(){ //When called, returns string for console
		return "Street: ";
	}
	static public String prompt_City(){ //When called, returns string for console
		return "City: ";
	}
	static public String prompt_State(){ //When called, returns string for console
		return "State: ";
	}
	static public String prompt_Zip(){ //When called, returns string for console
		return "Zip: ";
	};
	static public String prompt_Telephone(){ //When called, returns string for console
		return "Telephone: ";
	}
	static public String prompt_Email(){ //When called, returns string for console
		return "Email: ";
	}
}
